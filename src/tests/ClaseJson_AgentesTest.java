package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import elagente86.Agente;


public class ClaseJson_AgentesTest {
	private String archivoJson;
	private Gson gson;
	private ArrayList<Agente> agentes;

	@Before
	public void setUp() {
		gson = new GsonBuilder().setPrettyPrinting().create();
		archivoJson = gson.toJson("archivoJsonTest.json");
		agentes = new ArrayList<Agente>();
	}

	@Test
	public void testNull() {
		assertNotNull(gson);
		assertNotNull(archivoJson);
		assertTrue(agentes.isEmpty());
	}

	@Test
	public void testBordes() {
		agentes.add(new Agente("James", "Bond", 83.2, new Coordinate(-34.452, -58.7800), 45, " Z�rich", "Agente 007",
				1.84, "15/11/1940", "Masculino",new ImageIcon("imagenes/jamesBond.jpg")));
		agentes.add(new Agente("Ethan", "Hunt", 80.5, new Coordinate(-34.523, -58.7000), 47, "San Francisco", " - ",
				1.80, "1/1/1962", "Masculino",new ImageIcon("imagenes/Ethan-Hunt.jpg")));
		agentes.add(new Agente("Perry", "platypus", 84.6, new Coordinate(-34.640, -58.6200), 13, "Danville", "Agente P",
				0.40, "1/2/2008", "Masculino",new ImageIcon("imagenes/perry.png")));
		
		assertNotNull(agentes);
		assertNotNull(agentes.get(0).getFoto());
		assertEquals(3, agentes.size());
		assertEquals("James", agentes.get(0).getNombre());
		assertEquals(80.5, agentes.get(1).getPeso(), 80.5);
		agentes.remove(0);
		assertEquals(2, agentes.size());
		assertNotEquals("James", agentes.get(0).getNombre());
		assertEquals(80.5, agentes.get(0).getPeso(), 80.5);
		agentes.removeAll(agentes);
		assertEquals(0, agentes.size());
	}

}
