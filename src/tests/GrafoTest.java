package tests;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import elagente86.Grafo;

public class GrafoTest {
	Grafo grafotest= new Grafo(6);
	double[][] matriz = new double[6][6];
	
	@Before
	public void inicializar(){
		
		for (int i = 0; i < matriz.length; i++)
		{
			for (int f = 0; f < matriz.length; f++)
			{
				matriz[i][f] = 1;
			}
		}
	}
	
	@Test
	public void testBordes() {
		assertEquals(6, grafotest.getVertices());
		assertTrue(grafotest.getCamino().isEmpty());
		assertEquals(6, matriz.length);
		grafotest.caminoPrim(matriz);
		assertFalse(grafotest.getCamino().isEmpty());
	}

}
