package elagente86;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ClaseJson {
	private String archivoJson;

	public ClaseJson(String ruta) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		archivoJson = gson.toJson(ruta);
	}

	public void guardarAgentes(ArrayList<Agente> agentes) {
		JsonArray listaJsonAgentes = new JsonArray();
		
		for (int i = 0; i < agentes.size(); i++) {
			JsonObject object = new JsonObject();
			object.addProperty("nombre", agentes.get(i).getNombre());
			object.addProperty("apellido", agentes.get(i).getApellido());
			object.addProperty("peso", agentes.get(i).getPeso());
			object.addProperty("edad", agentes.get(i).getEdad());
			object.addProperty("ciudad Natal", agentes.get(i).getCiudadNatal());
			object.addProperty("nombre Clave", agentes.get(i).getNombreClave());
			object.addProperty("altura", agentes.get(i).getAltura());
			object.addProperty("fecha de nacimiento", agentes.get(i).getFechaNacimiento());
			object.addProperty("sexo", agentes.get(i).getSexo());
			object.addProperty("coordenadaLatitud", agentes.get(i).getUbicacion().getLat());
			object.addProperty("coordenadaLongitud", agentes.get(i).getUbicacion().getLon());
			object.addProperty("foto", agentes.get(i).getFoto().toString());
			listaJsonAgentes.add(object);

		}
		System.out.println("archivo: " + archivoJson);
		System.out.println(listaJsonAgentes.toString());
		try {
			FileWriter file = new FileWriter("Agentes.json");
			file.write(listaJsonAgentes.toString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("hiding")
	public ArrayList<Agente> cargarAgentes() {
		JsonParser parser = new JsonParser();
		ArrayList<Agente> agentes = new ArrayList<Agente>();

		try {

			Object objetoJson = parser.parse(new FileReader("Agentes.json"));

			JsonArray listaJson = (JsonArray) objetoJson;
			for (int i = 0; i < listaJson.size(); i++) {

				JsonObject objetoAgente = (JsonObject) listaJson.get(i);
				String nombre = objetoAgente.get("nombre").getAsString();
				String apellido = objetoAgente.get("apellido").getAsString();
				double peso = objetoAgente.get("peso").getAsDouble();
				int edad=objetoAgente.get("edad").getAsInt();
				String ciudadNatal=objetoAgente.get("ciudad Natal").getAsString();
				String nombreClave=objetoAgente.get("nombre Clave").getAsString();
				double altura=objetoAgente.get("altura").getAsDouble();
				String fechanac=objetoAgente.get("fecha de nacimiento").getAsString();
				String sexo=objetoAgente.get("sexo").getAsString();
				double coordenadaLatitud = objetoAgente.get("coordenadaLatitud").getAsDouble();
				double coordenadaLongitud = objetoAgente.get("coordenadaLongitud").getAsDouble();
				Coordinate ubicacion = new Coordinate(coordenadaLatitud, coordenadaLongitud);
				ImageIcon foto = new ImageIcon(objetoAgente.get("foto").getAsString());
				Agente agent = new Agente(nombre, apellido, peso, ubicacion, edad, ciudadNatal, nombreClave, altura, fechanac, sexo, foto);

				agentes.add(agent);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return agentes;
	}

	public String getArchivoJson() {
		return archivoJson;
	}

	public void setArchivoJson(String archivoJson) {
		this.archivoJson = archivoJson;
	}
}
