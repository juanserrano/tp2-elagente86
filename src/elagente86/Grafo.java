package elagente86;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Grafo {
    private int vertices;
    private ArrayList<Integer> camino;
    private int pesoTotal;
	private int aristaMin;
    private int aristaMax;
    private double desviacion;
    
    public Grafo(int size){
    	this.vertices = size;
    	camino = new ArrayList<Integer>();
    	pesoTotal = 0;
    	aristaMax = 0;
    	aristaMin = Integer.MAX_VALUE;
    	desviacion = 0;
    }
    
    public ArrayList<Integer> caminoPrim(double matriz[][])
    {
    	
    	
        int parent[] = new int[vertices]; 
        int distancia[] = new int [vertices]; 
        Boolean verticesUsados[] = new Boolean[vertices]; 
  
        for (int i = 0; i < vertices; i++)
        { 
            distancia[i] = Integer.MAX_VALUE;//infinito
            verticesUsados[i] = false; 
        }
        
        distancia[0] = 0;//primer nodo 
        parent[0] = -1;
  
        for (int cont = 0; cont < vertices-1; cont++) 
        { 

            int u = distanciaMinima(distancia, verticesUsados);
  
            verticesUsados[u] = true;
   
            for (int v = 0; v < vertices; v++)
                if (matriz[u][v]!=0 && verticesUsados[v] == false && matriz[u][v] < distancia[v]) 
                {
                    parent[v] = u; 
                    distancia[v] = (int) matriz[u][v];
                } 
        }
        printMST(parent, vertices, matriz);
        
        return camino;
    }
    

    int distanciaMinima(int distancia[], Boolean verticesUsados[]) 
    { 
    	
        int min = Integer.MAX_VALUE;
        int min_index=-1; 
  
        for (int v = 0; v < vertices; v++) 
            if (verticesUsados[v] == false && distancia[v] < min)
            { 
                min = distancia[v];
                min_index = v; 
            } 
  
        return min_index;
    } 
    
    void printMST(int parent[], int n, double[][] matriz) 
    { 
    	DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("Vertice  Kilometros");
        for (int i = 1; i < vertices; i++)
        {
            System.out.println(parent[i]+" - "+ i+"\t "+ df.format(matriz[i][parent[i]]));
        	camino.add(parent[i]);
        	camino.add(i);
        	pesoTotal += matriz[i][parent[i]];
        	desviacion += Math.pow(matriz[i][parent[i]], 2);
        	aristaMax = (aristaMax < matriz[i][parent[i]]) ? (int) matriz[i][parent[i]] : aristaMax;
        	aristaMin = (aristaMin > matriz[i][parent[i]]) ? (int) matriz[i][parent[i]] : aristaMin;
        }
        desviacion = Math.sqrt(((desviacion/5)-pesoTotal/vertices));
    }
    
    public int getVertices() {
		return vertices;
	}
    
    public void setVertices(int vertices) {
		this.vertices = vertices;
	}

	public ArrayList<Integer> getCamino() {
		return camino;
	}

	public void setCamino(ArrayList<Integer> camino) {
		this.camino = camino;
	}

	public int getPesoTotal() {
		return pesoTotal;
	}

	public void setPesoTotal(int pesoTotal) {
		this.pesoTotal = pesoTotal;
	}

	public int getAristaMin() {
		return aristaMin;
	}

	public void setAristaMin(int aristaMin) {
		this.aristaMin = aristaMin;
	}

	public int getAristaMax() {
		return aristaMax;
	}

	public void setAristaMax(int aristaMax) {
		this.aristaMax = aristaMax;
	}
	
	public double getDesviacionl() {
		return desviacion;
	}

	public void setDesviacion(double desviacion){
		this.desviacion = desviacion;
	}
	
	public double getCostoAristaPromedio() {
		return pesoTotal/vertices;
	}
    

} 