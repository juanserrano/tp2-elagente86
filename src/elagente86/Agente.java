package elagente86;

import javax.swing.ImageIcon;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Agente {
	private String nombre;
	private String apellido;
	private int edad;
	private String CiudadNatal;
	private ImageIcon foto;
	private String nombreClave;
	private double peso;
	private double altura;
	private String fechaNacimiento;
	private Coordinate ubicacion;
	private String sexo;

	public Agente(String nombre, String apellido, double peso, Coordinate ubicacion, int edad, String ciudadNatal,
			String nombreClave, double altura, String fechaNacimiento, String sexo, ImageIcon foto) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.CiudadNatal = ciudadNatal;
		this.foto = foto;
		this.nombreClave = nombreClave;
		this.peso = peso;
		this.altura = altura;
		this.fechaNacimiento = fechaNacimiento;
		this.ubicacion = ubicacion;
		this.setSexo(sexo);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiudadNatal() {
		return CiudadNatal;
	}

	public void setCiudadNatal(String paisNatal) {
		this.CiudadNatal = paisNatal;
	}

	public ImageIcon getFoto() {
		return foto;
	}

	public void setFoto(ImageIcon foto) {
		this.foto = foto;
	}

	public String getNombreClave() {
		return nombreClave;
	}

	public void setNombreClave(String nombreClave) {
		this.nombreClave = nombreClave;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Coordinate getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Coordinate ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

}
