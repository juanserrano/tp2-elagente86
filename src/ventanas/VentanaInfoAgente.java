package ventanas;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.SwingConstants;
import java.awt.Font;



import javax.swing.JButton;
import javax.swing.border.BevelBorder;

import elagente86.Agente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.awt.Color;

@SuppressWarnings("serial")
public class VentanaInfoAgente extends javax.swing.JFrame {
	private Agente agent;

	public VentanaInfoAgente(Agente agent) {
		this.agent = agent;
		initialize();
	}

	private void initialize() {
		this.setResizable(false);
		this.setTitle("El Agente 86");
		this.setBounds(500, 200, 800, 600);
		this.getContentPane().setLayout(null);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/logoungs.png"));

		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.setBounds(212, 0, 582, 518);
		this.getContentPane().add(panel);
		panel.setLayout(new GridLayout(9, 2, 0, 0));

		JLabel lblNewLabel_1 = new JLabel("Nombre: ");
		lblNewLabel_1.setForeground(Color.BLACK);
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel_1);

		JLabel labelnombre = new JLabel("");
		labelnombre.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelnombre.setHorizontalAlignment(SwingConstants.CENTER);
		labelnombre.setText(agent.getNombre());
		panel.add(labelnombre);

		JLabel lblNewLabel_4 = new JLabel("Apellido:");
		lblNewLabel_4.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel_4);

		JLabel labelap = new JLabel("");
		labelap.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelap.setHorizontalAlignment(SwingConstants.CENTER);
		labelap.setText(agent.getApellido());
		panel.add(labelap);

		JLabel lblEdad = new JLabel("Edad: ");
		lblEdad.setHorizontalAlignment(SwingConstants.CENTER);
		lblEdad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(lblEdad);

		JLabel labeledad = new JLabel("");
		labeledad.setHorizontalAlignment(SwingConstants.CENTER);
		labeledad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labeledad.setText("" + agent.getEdad());
		panel.add(labeledad);

		JLabel lblNewLabel_2 = new JLabel("Peso: ");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Sitka Text", Font.PLAIN, 16));

		panel.add(lblNewLabel_2);

		JLabel labelpeso = new JLabel("");
		labelpeso.setHorizontalAlignment(SwingConstants.CENTER);
		labelpeso.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelpeso.setText(agent.getPeso() + "");
		panel.add(labelpeso);

		JLabel lblNewLabel_8 = new JLabel("Ciudad natal:");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_8.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(lblNewLabel_8);

		JLabel labelciudad = new JLabel("");
		labelciudad.setHorizontalAlignment(SwingConstants.CENTER);
		labelciudad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelciudad.setText(agent.getCiudadNatal());
		panel.add(labelciudad);

		JLabel lblNewLabel_12 = new JLabel("Nombre clave: ");
		lblNewLabel_12.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_12.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(lblNewLabel_12);

		JLabel labelnomclave = new JLabel("");
		labelnomclave.setHorizontalAlignment(SwingConstants.CENTER);
		labelnomclave.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelnomclave.setText(agent.getNombreClave());
		panel.add(labelnomclave);

		JLabel lblNewLabel_14 = new JLabel("Altura: ");
		lblNewLabel_14.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_14.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(lblNewLabel_14);

		JLabel labelaltura = new JLabel("");
		labelaltura.setHorizontalAlignment(SwingConstants.CENTER);
		labelaltura.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelaltura.setText("" + agent.getAltura());
		panel.add(labelaltura);

		JLabel lblNewLabel_11 = new JLabel("Fecha de nacimiento: ");
		lblNewLabel_11.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		lblNewLabel_11.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel_11);

		JLabel labelfecha = new JLabel("");
		labelfecha.setHorizontalAlignment(SwingConstants.CENTER);
		labelfecha.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelfecha.setText(agent.getFechaNacimiento());
		panel.add(labelfecha);

		JLabel lblNewLabel = new JLabel("Sexo: ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(lblNewLabel);

		JLabel labelSexo = new JLabel("");
		labelSexo.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelSexo.setHorizontalAlignment(SwingConstants.CENTER);
		labelSexo.setText(agent.getSexo());
		panel.add(labelSexo);

		JLabel labelFoto = new JLabel("");
		labelFoto.setIcon(agent.getFoto());
		labelFoto.setHorizontalAlignment(SwingConstants.CENTER);
		labelFoto.setBounds(0, 0, 209, 518);
		this.getContentPane().add(labelFoto);

		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		btnNewButton.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		btnNewButton.setBounds(0, 537, 209, 34);
		this.getContentPane().add(btnNewButton);
	}
}
