package ventanas;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class PrimerVentana extends javax.swing.JFrame {

	private JLabel labelWelcome;
	private JTextField textoUsuario;
	private JTextField textoClave;
	private VentanaPrincipal segundaVentana;

	public PrimerVentana() {
		setTitle("El Agente 86");
		initialize();
	}

	private void initialize() {
		this.setResizable(false);
		this.setBounds(100, 100, 609, 415);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/logoungs.png"));

		labelWelcome = new JLabel("Bienvenido al sistema");
		labelWelcome.setFont(new Font("Sitka Text", Font.PLAIN, 22));
		labelWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		labelWelcome.setBounds(0, 11, 603, 32);
		this.getContentPane().add(labelWelcome);

		JLabel labelident = new JLabel("Por favor, identifiquese:");
		labelident.setHorizontalAlignment(SwingConstants.CENTER);
		labelident.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelident.setBounds(10, 55, 593, 25);
		this.getContentPane().add(labelident);

		JLabel labelUsuario = new JLabel("Usuario: ");
		labelUsuario.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		labelUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		labelUsuario.setBounds(10, 114, 90, 25);
		this.getContentPane().add(labelUsuario);

		JLabel labelClave = new JLabel("Clave: ");
		labelClave.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		labelClave.setHorizontalAlignment(SwingConstants.CENTER);
		labelClave.setBounds(10, 207, 90, 17);
		this.getContentPane().add(labelClave);

		textoUsuario = new JTextField();
		textoUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textoUsuario.setBounds(143, 116, 160, 20);
		this.getContentPane().add(textoUsuario);
		textoUsuario.setColumns(10);

		textoClave = new JTextField();
		textoClave.setBackground(Color.WHITE);
		textoClave.setForeground(Color.WHITE);
		textoClave.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textoClave.setBounds(143, 205, 160, 20);
		this.getContentPane().add(textoClave);
		textoClave.setColumns(10);

		JButton botonAcceder = new JButton("Acceder");
		botonAcceder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textoUsuario.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, " Has accedido al sistema correctamente ", " El Agente 86 ",
						JOptionPane.INFORMATION_MESSAGE);
				segundaVentana= new VentanaPrincipal(textoUsuario.getText());
				setVisible(false);
				segundaVentana.setVisible(true);}
				else
					JOptionPane.showMessageDialog(null, " Por favor ingresa un usuario ", " El Agente 86 ",
							JOptionPane.CANCEL_OPTION);
			}
		});
		botonAcceder.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		botonAcceder.setBounds(30, 323, 89, 32);
		this.getContentPane().add(botonAcceder);

		JButton botonSalir = new JButton("Salir");
		botonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		botonSalir.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		botonSalir.setBounds(424, 323, 89, 32);
		this.getContentPane().add(botonSalir);
	}
}
