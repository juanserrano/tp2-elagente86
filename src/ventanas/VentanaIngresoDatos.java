package ventanas;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.SwingConstants;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

import elagente86.Agente;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class VentanaIngresoDatos extends javax.swing.JFrame {
	private JTextField textFieldNombre;
	private JTextField textFieldEdad;
	private JTextField textFieldApellido;
	private JTextField textFieldPeso;
	private JTextField textFieldCiudad;
	private JTextField textFieldClave;
	private JTextField textFieldAltura;
	private JTextField textFieldSexo;
	private JTextField textFieldFechaNac;
	private VentanaPrincipal ventana;
	private Coordinate ubicacion;

	public VentanaIngresoDatos(VentanaPrincipal ventanaPrincipal, Coordinate ubicacion) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.ubicacion = ubicacion;
		this.ventana = ventanaPrincipal;
		initialize();
	}

	private void initialize() {
		this.setResizable(false);
		this.setTitle("El Agente 86");
		this.setBounds(500, 200, 800, 600);
		this.getContentPane().setLayout(null);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/logoungs.png"));

		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.setBounds(212, 44, 582, 527);
		this.getContentPane().add(panel);
		panel.setLayout(new GridLayout(9, 2, 0, 0));

		JLabel labelNombre = new JLabel("Nombre: ");
		labelNombre.setForeground(Color.BLACK);
		labelNombre.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelNombre.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(labelNombre);

		textFieldNombre = new JTextField();
		textFieldNombre.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldNombre.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel labelApellido = new JLabel("Apellido:");
		labelApellido.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelApellido.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(labelApellido);

		textFieldApellido = new JTextField();
		textFieldApellido.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldApellido.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldApellido);
		textFieldApellido.setColumns(10);

		JLabel labelEdad = new JLabel("Edad: ");
		labelEdad.setHorizontalAlignment(SwingConstants.CENTER);
		labelEdad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(labelEdad);

		textFieldEdad = new JTextField();
		textFieldEdad.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldEdad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldEdad);
		textFieldEdad.setColumns(10);

		JLabel labelPeso = new JLabel("Peso: ");
		labelPeso.setHorizontalAlignment(SwingConstants.CENTER);
		labelPeso.setFont(new Font("Sitka Text", Font.PLAIN, 16));

		panel.add(labelPeso);

		textFieldPeso = new JTextField();
		textFieldPeso.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldPeso.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldPeso);
		textFieldPeso.setColumns(10);

		JLabel labelCiudad = new JLabel("Ciudad natal:");
		labelCiudad.setHorizontalAlignment(SwingConstants.CENTER);
		labelCiudad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(labelCiudad);

		textFieldCiudad = new JTextField();
		textFieldCiudad.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldCiudad.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldCiudad);
		textFieldCiudad.setColumns(10);

		JLabel labelClave = new JLabel("Nombre clave: ");
		labelClave.setHorizontalAlignment(SwingConstants.CENTER);
		labelClave.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(labelClave);

		textFieldClave = new JTextField();
		textFieldClave.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldClave.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldClave);
		textFieldClave.setColumns(10);

		JLabel labelAltura = new JLabel("Altura: ");
		labelAltura.setHorizontalAlignment(SwingConstants.CENTER);
		labelAltura.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(labelAltura);

		textFieldAltura = new JTextField();
		textFieldAltura.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldAltura.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldAltura);
		textFieldAltura.setColumns(10);

		JLabel labelSexo = new JLabel("Sexo: ");
		labelSexo.setHorizontalAlignment(SwingConstants.CENTER);
		labelSexo.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(labelSexo);

		textFieldSexo = new JTextField();
		textFieldSexo.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldSexo.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldSexo);
		textFieldSexo.setColumns(10);

		JLabel labelFechaNac = new JLabel("Fecha de nacimiento: ");
		labelFechaNac.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelFechaNac.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(labelFechaNac);

		textFieldFechaNac = new JTextField();
		textFieldFechaNac.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldFechaNac.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panel.add(textFieldFechaNac);
		textFieldFechaNac.setColumns(10);

		
		
		JButton botonAceptar = new JButton("Elegir foto y Terminar");
		botonAceptar.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent arg0) {

				if (chequearTextosVacios()) {
					try {
						Agente agente = new Agente(getTextFieldNombre(), getTextFieldApellido(), getTextFieldPeso(),
								ubicacion, getTextFieldEdad(), getTextFieldCiudad(), getTextFieldClave(),
								getTextFieldAltura(), getTextFieldFechaNac(), getTextFieldSexo(), fotoPerfil(botonAceptar));
						ventana.getAgentes().add(agente);
						ventana.MarcarAgenteYGuardar(agente);
						setVisible(false);

					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, " Error en la escritura de la información ",
								" El agente 86", JOptionPane.ERROR_MESSAGE);
					}
				} else
					JOptionPane.showMessageDialog(null, " No puedes dejar datos en blanco ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
			}

		});
		
		
		
		
				JLabel labelFoto = new JLabel("");
				labelFoto.setEnabled(false);
				// labelFoto.setIcon(agent.getFoto());
				labelFoto.setHorizontalAlignment(SwingConstants.CENTER);
				labelFoto.setBounds(0, 44, 209, 474);
				this.getContentPane().add(labelFoto);

		botonAceptar.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		botonAceptar.setBounds(0, 537, 209, 34);
		this.getContentPane().add(botonAceptar);

		JPanel panelSuperior = new JPanel();
		panelSuperior.setBounds(0, 0, 794, 45);
		getContentPane().add(panelSuperior);
		panelSuperior.setLayout(null);

		JLabel labelInicial = new JLabel(
				"Por favor, llene la siguiente planilla con la informaci\u00F3n del agente a ubicar:   ");
		labelInicial.setHorizontalAlignment(SwingConstants.CENTER);
		labelInicial.setFont(new Font("Sitka Text", Font.PLAIN, 18));
		labelInicial.setBounds(0, 0, 794, 45);
		panelSuperior.add(labelInicial);
	}

	public ImageIcon fotoPerfil(JButton n){
		JFileChooser fileChooser = new JFileChooser("imagenes");;
		int seleccion = fileChooser.showSaveDialog(n);
		
		if (seleccion == JFileChooser.APPROVE_OPTION)
		{
			FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & PNG", "jpg", "PNG");
			fileChooser.setFileFilter(filter);
			File fichero = fileChooser.getSelectedFile();
		   
		   return new ImageIcon(fichero.toString());
		}
		return null;
	}
	
	
	private boolean chequearTextosVacios() {
		if (textFieldNombre.getText().equals("") || textFieldApellido.getText().equals("")
				|| textFieldPeso.getText().equals("") || textFieldEdad.getText().equals("")
				|| textFieldCiudad.getText().equals("") || textFieldClave.getText().equals("")
				|| textFieldAltura.getText().equals("") || textFieldFechaNac.getText().equals("")
				|| textFieldSexo.getText().equals("")) {
			return false;
		}

		return true;
	}

	public double getTextFieldPeso() {
		return Double.parseDouble(textFieldPeso.getText());
	}

	public void setTextFieldPeso(JTextField textFieldPeso) {
		this.textFieldPeso = textFieldPeso;
	}

	public String getTextFieldCiudad() {
		return textFieldCiudad.getText();
	}

	public void setTextFieldCiudad(JTextField textFieldCiudad) {
		this.textFieldCiudad = textFieldCiudad;
	}

	public String getTextFieldClave() {
		return textFieldClave.getText();
	}

	public void setTextFieldClave(JTextField textFieldClave) {
		this.textFieldClave = textFieldClave;
	}

	public Double getTextFieldAltura() {
		return Double.parseDouble(textFieldAltura.getText());
	}

	public void setTextFieldAltura(JTextField textFieldAltura) {
		this.textFieldAltura = textFieldAltura;
	}

	public String getTextFieldSexo() {
		return textFieldSexo.getText();
	}

	public void setTextFieldSexo(JTextField textFieldSexo) {
		this.textFieldSexo = textFieldSexo;
	}

	public String getTextFieldFechaNac() {
		return textFieldFechaNac.getText();
	}

	public void setTextFieldFechaNac(JTextField textFieldFechaNac) {
		this.textFieldFechaNac = textFieldFechaNac;
	}

	public String getTextFieldNombre() {
		return textFieldNombre.getText();
	}

	public void setTextFieldNombre(JTextField textFieldNombre) {
		this.textFieldNombre = textFieldNombre;
	}

	public int getTextFieldEdad() {
		return Integer.parseInt(textFieldEdad.getText());
	}

	public void setTextFieldEdad(JTextField textFieldEdad) {
		this.textFieldEdad = textFieldEdad;
	}

	public String getTextFieldApellido() {
		return textFieldApellido.getText();
	}

	public void setTextFieldApellido(JTextField textFieldApellido) {
		this.textFieldApellido = textFieldApellido;
	}
}
