package ventanas;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import elagente86.Agente;
import elagente86.ClaseJson;
import elagente86.Grafo;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JComboBox;

import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class VentanaPrincipal extends javax.swing.JFrame {

	private JPanel panelMapa;
	private JMapViewer mapa;
	private ClaseJson agentesJson;
	private ArrayList<Agente> agentes;
	private ArrayList<Integer> camino;
	private Grafo grafoPrim;
	private String nombreUsuario;
	private VentanaInfoAgente windowDataAgent;
	private VentanaIngresoDatos ventanaPlanillaAgente;
	private JLabel lblintPesoTotal;
	private JLabel lblintMaxArista;
	private JLabel lblintAristaMin;
	private JLabel lblIntcostoprom;
	private JLabel lblIntdesviacion;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox;

	public VentanaPrincipal(String nombreIngresante) {
		this.nombreUsuario = nombreIngresante;
		this.setResizable(false);
		this.setTitle(" El Agente 86");
		this.getContentPane().setLayout(new BorderLayout(0, 0));

		panelMapa = new JPanel();
		getContentPane().add(panelMapa, BorderLayout.CENTER);
		panelMapa.setLayout(null);
		agentesJson = new ClaseJson("Agentes.json");
		agentes = new ArrayList<Agente>();
		initialize();
	}

	@SuppressWarnings({ "rawtypes" })
	private void initialize() {

		this.setBounds(500, 200, 900, 750);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/logoungs.png"));

		mapa = new JMapViewer();
		mapa.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		mapa.setBounds(0, 41, 737, 637);
		mapa.setDisplayPosition(new Coordinate(-34.521, -58.7008), 10);

		panelMapa.setLayout(null);
		panelMapa.add(mapa);

		JPanel panelNombreAcceso = new JPanel();
		panelNombreAcceso.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelNombreAcceso.setBounds(0, 0, 894, 42);
		panelMapa.add(panelNombreAcceso);
		panelNombreAcceso.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel labelIngreso = new JLabel("Ingresaste al sistema como: ");
		labelIngreso.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelIngreso.setHorizontalAlignment(SwingConstants.CENTER);
		panelNombreAcceso.add(labelIngreso);

		JLabel labelNombre = new JLabel("");
		labelNombre.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		labelNombre.setHorizontalAlignment(SwingConstants.LEFT);
		labelNombre.setText(nombreUsuario);
		panelNombreAcceso.add(labelNombre);

		JPanel panelBotones = new JPanel();
		panelBotones.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelBotones.setBounds(0, 679, 894, 42);
		panelMapa.add(panelBotones);
		panelBotones.setLayout(new GridLayout(0, 2, 0, 0));

		JButton botonGrafo = new JButton("Visualizar Grafo completo");
		botonGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!agentes.isEmpty())
					dibujarGrafoCompleto();
				else
					JOptionPane.showMessageDialog(null, " Primero debes ubicar los agentes ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
			}
		});
		botonGrafo.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelBotones.add(botonGrafo);

		JButton botonAGM = new JButton("Visualizar AGM");
		botonAGM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!agentes.isEmpty()) {
					armarCamino();
					dibujarCamino();
					mostrarDatosDelAGM();
				} else
					JOptionPane.showMessageDialog(null, " Primero debes ubicar los agentes ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
			}
		});
		botonAGM.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelBotones.add(botonAGM);

		comboBox = new JComboBox();
		comboBox.setBackground(Color.WHITE);
		comboBox.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		comboBox.setBounds(738, 142, 156, 35);
		panelMapa.add(comboBox);

		JButton botonInfoAgente = new JButton("Ver info de agente");
		botonInfoAgente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!agentes.isEmpty()) {
					windowDataAgent = new VentanaInfoAgente(agentes.get(comboBox.getSelectedIndex()));
					windowDataAgent.setVisible(true);
				} else
					JOptionPane.showMessageDialog(null, " No hay ningun agente a mostrar ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
			}
		});

		botonInfoAgente.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		botonInfoAgente.setBounds(738, 41, 156, 90);
		panelMapa.add(botonInfoAgente);

		JLabel lblPesoTotal = new JLabel("Peso total:");
		lblPesoTotal.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		lblPesoTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblPesoTotal.setBounds(747, 206, 60, 14);
		panelMapa.add(lblPesoTotal);

		lblintPesoTotal = new JLabel("");
		lblintPesoTotal.setBounds(817, 206, 57, 14);
		lblintPesoTotal.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelMapa.add(lblintPesoTotal);

		JLabel lblMaxArista = new JLabel("Max Arista:");
		lblMaxArista.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		lblMaxArista.setHorizontalAlignment(SwingConstants.CENTER);
		lblMaxArista.setBounds(747, 231, 60, 14);
		panelMapa.add(lblMaxArista);

		lblintMaxArista = new JLabel("");
		lblintMaxArista.setBounds(817, 230, 57, 14);
		lblintMaxArista.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelMapa.add(lblintMaxArista);

		JLabel lblMinArista = new JLabel("Min Arista:");
		lblMinArista.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		lblMinArista.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinArista.setBounds(747, 256, 60, 14);
		panelMapa.add(lblMinArista);

		lblintAristaMin = new JLabel("");
		lblintAristaMin.setBounds(817, 255, 57, 14);
		lblintAristaMin.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelMapa.add(lblintAristaMin);

		JLabel lblCostoProm = new JLabel("Costo prom:");
		lblCostoProm.setFont(new Font("Dialog", Font.PLAIN, 10));
		lblCostoProm.setBounds(747, 281, 66, 14);
		panelMapa.add(lblCostoProm);

		lblIntcostoprom = new JLabel("");
		lblIntcostoprom.setBounds(817, 281, 57, 14);
		lblIntcostoprom.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelMapa.add(lblIntcostoprom);

		JLabel lblDesviacion = new JLabel("Desviaci\u00F3n:");
		lblDesviacion.setFont(new Font("Dialog", Font.PLAIN, 10));
		lblDesviacion.setBounds(747, 306, 60, 14);
		panelMapa.add(lblDesviacion);

		lblIntdesviacion = new JLabel("");
		lblIntdesviacion.setBounds(817, 306, 67, 14);
		lblIntdesviacion.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		panelMapa.add(lblIntdesviacion);

		JButton btnNewButton = new JButton(" Guardar agentes ");
		btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 14));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!agentes.isEmpty()) {
					agentesJson.guardarAgentes(agentes);
					JOptionPane.showMessageDialog(null,
							" Los agentes han sido guardados correctamente en un archivo JSON ", " El Agente 86",
							JOptionPane.INFORMATION_MESSAGE);
				} else
					JOptionPane.showMessageDialog(null, " No hay ningun agente a guardar ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
			}
		});
		btnNewButton.setBounds(738, 364, 156, 35);
		panelMapa.add(btnNewButton);
		
		JButton button = new JButton(" Cargar agentes ");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!agentesJson.cargarAgentes().isEmpty() && agentes.isEmpty()) {
					agentes = agentesJson.cargarAgentes();
					for (Agente a : agentes) {
						MarcarAgenteYGuardar(a);
					}

					JOptionPane.showMessageDialog(null,
							" Los agentes han sido cargados correctamente en el mapa ", " El Agente 86",
							JOptionPane.INFORMATION_MESSAGE);
				} 
				else if(agentesJson.cargarAgentes().isEmpty())
					JOptionPane.showMessageDialog(null, " No hay ningun agente a cargar ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
				else
					JOptionPane.showMessageDialog(null, " No puedes cargar agentes si ya hay agentes ubicados en el mapa ", " El agente 86",
							JOptionPane.CANCEL_OPTION);
			}
		});
		button.setFont(new Font("Dialog", Font.PLAIN, 14));
		button.setBounds(738, 410, 156, 35);
		panelMapa.add(button);

		
		
		mapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					Coordinate posicion = (Coordinate)mapa.getPosition(e.getPoint());
					llenarPlanillaAgente(posicion);
				}
			}
		});

	}

	private void llenarPlanillaAgente(Coordinate ubicacion) {
		ventanaPlanillaAgente = new VentanaIngresoDatos(this, ubicacion);
		ventanaPlanillaAgente.setVisible(true);
	}

	public static double distanciaCoord(double lat1, double long1, double lat2, double long2) {
		double radioTierra = 6371;// en kilómetros
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(long2 - long1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double va1 = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;

		return distancia;
	}

	private static double[][] generarMatriz(ArrayList<Agente> agentes) {
		double[][] matriz = new double[agentes.size()][agentes.size()];
		for (int i = 0; i < agentes.size(); i++) {
			for (int f = 0; f < agentes.size(); f++) {
				matriz[i][f] = distanciaCoord(agentes.get(i).getUbicacion().getLat(),
						agentes.get(i).getUbicacion().getLon(), agentes.get(f).getUbicacion().getLat(),
						agentes.get(f).getUbicacion().getLon());
			}
		}
		return matriz;
	}

	public void printMatriz(double[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int f = 0; f < matriz.length; f++) {
				System.out.print(matriz[i][f]);
				System.out.print("|");
			}
			System.out.println();
		}
	}

	private void armarCamino() {
		grafoPrim = new Grafo(agentes.size());
		double[][] matriz = generarMatriz(agentes);
		camino = grafoPrim.caminoPrim(matriz);
	}

	private void dibujarGrafoCompleto() {
		int i = 1;
		for (Agente agent : agentes) {
			for (int cont = i; cont < agentes.size(); cont++) {
				MapPolygonImpl pol = new MapPolygonImpl(agent.getUbicacion(), agentes.get(cont).getUbicacion(),
						agent.getUbicacion());
				mapa.addMapPolygon(pol);
			}
			i++;
		}
	}

	private void dibujarCamino() {
		mapa.removeAllMapPolygons();
		for (int i = 0; i < camino.size(); i += 2) {
			MapPolygonImpl linea = new MapPolygonImpl(agentes.get(camino.get(i)).getUbicacion(),
					agentes.get(camino.get(i + 1)).getUbicacion(), agentes.get(camino.get(i)).getUbicacion());
			mapa.addMapPolygon(linea);

		}
	}

	private void mostrarDatosDelAGM() {
		lblintPesoTotal.setText(String.valueOf(grafoPrim.getPesoTotal()));
		lblintMaxArista.setText(String.valueOf(grafoPrim.getAristaMax()));
		lblintAristaMin.setText(String.valueOf(grafoPrim.getAristaMin()));
		lblIntcostoprom.setText(String.valueOf(grafoPrim.getCostoAristaPromedio()));
		DecimalFormat df = new DecimalFormat("#.00");
		lblIntdesviacion.setText(df.format(grafoPrim.getDesviacionl()));

	}

	public ArrayList<Agente> getAgentes() {
		return agentes;
	}

	public void setAgentes(ArrayList<Agente> agentes) {
		this.agentes = agentes;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getComboBox() {
		return comboBox;
	}

	@SuppressWarnings("rawtypes")
	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JMapViewer getMapa() {
		return mapa;
	}

	public void setMapa(JMapViewer mapa) {
		this.mapa = mapa;
	}
	
	public void MarcarAgenteYGuardar(Agente a) {
		getComboBox().addItem(a.getNombre() + " " + a.getApellido());
		getMapa().addMapMarker(new MapMarkerDot(a.getNombre(), a.getUbicacion()));
	}
}
